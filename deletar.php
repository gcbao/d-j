<?php
	session_start();
	set_time_limit(0);
	ini_set('post_max_size', '10M');
	ini_set('upload_max_filesize', '10M');

	require "banco.php";
	$sql = new banco("127.0.0.1","root","","sistema_funcionarios");

	#Verifica se o Cliente que está logado está com seu código correto e da acesso a
	#página administrativa, caso contrario retorna para tela de login.
	if(!$_SESSION['SISTFUNC2015SYSTEM']) {
		header("Location: index.php");
		die;
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sistema de Funcionários</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="funcionarios.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Lista de Funcionários</a></li>
				<li class="label-success"><a href="cadastro.php" style="color: #FFF;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo Funcionário</a></li>				
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="label-danger"><a href="logout.php" style="color: #FFF;"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair</a></li>
			</ul>
		</div>
	</div>
</nav>


<div class="container">
	<h3 class="text-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Funcionário deletado com sucesso!</h3>
	<a href="funcionarios.php"><button class="btn btn-warning">Voltar para lista de Funcionários</button></a>
	<?php
		$s_user = "DELETE FROM tbl_funcionarios WHERE func_id = " . @$_REQUEST['id'];
		$q_user = mysqli_query($sql->AcessoBanco(),$s_user);
	?>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>