<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sistema de Funcionários</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<div class="container-fluid">
	<div class="col-lg-2">
		<h4>SISTEMA DE FUNCIONÁRIOS</h4>
		<form action="logado.php" method="post">
			<div class="form-group">
				<input type="text" class="form-control" name="ac_email" placeholder="Digite seu Usuário">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="ac_senha" placeholder="Digite sua Senha" maxlength="16">
			</div>
			<button type="submit" class="btn btn-info">Acessar</button>
		</form>
	</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>