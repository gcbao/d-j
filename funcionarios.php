<?php
	session_start();
	set_time_limit(0);
	ini_set('post_max_size', '10M');
	ini_set('upload_max_filesize', '10M');

	require "banco.php";
	$sql = new banco("127.0.0.1","root","","sistema_funcionarios");

	#Verifica se o Cliente que está logado está com seu código correto e da acesso a
	#página administrativa, caso contrario retorna para tela de login.
	if(!$_SESSION['SISTFUNC2015SYSTEM']) {
		header("Location: index.php");
		die;
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sistema de Funcionários</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="funcionarios.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Lista de Funcionários</a></li>
				<li class="label-success"><a href="cadastro.php" style="color: #FFF;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo Funcionário</a></li>				
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="label-danger"><a href="logout.php" style="color: #FFF;"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair</a></li>
			</ul>
		</div>
	</div>
</nav>


<div class="container">
	<div>
			<form action="funcionarios.php" method="get">
				<button class="btn btn-primary pull-right"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
				<div class="col-lg-2 pull-right">
					<select name="ordem" class="form-control col-lg-2">
						<option value="ASC">Crescente</option>
						<option value="DESC">Decrescente</option>
					</select>
				</div>
				<div class="col-lg-2 pull-right">
					<select name="filtro_a" class="form-control col-lg-2">
						<option value=""></option>
						<option value="func_nome">Nome</option>
						<option value="func_email">E-mail</option>
						<option value="func_setor">Setor</option>
						<option value="func_cargo">Cargo</option>
					</select>
				</div>
				<div class="col-lg-2 pull-right">			
					<select name="filtro_b" class="form-control">
						<option value=""></option>
						<option value="func_nome">Nome</option>
						<option value="func_email">E-mail</option>
						<option value="func_setor">Setor</option>
						<option value="func_cargo">Cargo</option>
					</select>
				</div>			
			</form>
			<div class="clearfix"></div>
			<hr>

		<table class="table table-hover table-striped">
			<thead>
				<tr class="label-primary" style="color: #FFF;">
					<th style="text-align: center;">#</th>
					<th style="text-align: center;">Nome</th>
					<th style="text-align: center;">E-mail</th>
					<th style="text-align: center;">Setor</th>
					<th style="text-align: center;">Cargo</th>
					<th style="text-align: center;">Edição</th>
				</tr>				
			</thead>
			<tbody>
				<?php
				$filtroA = $filtroB = $orderby = $ordem = "";

				if(@$_REQUEST['filtro_a'] != "" && @$_REQUEST['filtro_b'] == "") {
					$filtroA .= $_REQUEST['filtro_a'];
				}
				if(@$_REQUEST['filtro_b'] != "" && @$_REQUEST['filtro_a'] == "") {
					$filtroB .= $_REQUEST['filtro_b'];
				}

				if(@$_REQUEST['filtro_a'] != "" && @$_REQUEST['filtro_b'] != "") {
					$filtroA .= $_REQUEST['filtro_a'];
					$filtroB .= ", " . $_REQUEST['filtro_b'];
				}

				if(@$_REQUEST['filtro_a'] != "" || @$_REQUEST['filtro_b'] != "") {
					$orderby .= " ORDER BY ";
					$ordem .= " " . @$_REQUEST['ordem'];
				}
				$s_user = "SELECT * FROM tbl_funcionarios" . $orderby . " " . $filtroA . " " . $filtroB . " " . $ordem;
				$q_user = mysqli_query($sql->AcessoBanco(),$s_user);

				while ($u = mysqli_fetch_array($q_user)) 
				{
					?>
					<tr style="text-align: center;">
						<th scope="row"><img src="images/<?php echo $u['func_foto']; ?>" style="width: 48px;" class="center-block"></th>
						<td style="vertical-align: middle;"><?php echo $u['func_nome']; ?></td>
						<td style="vertical-align: middle;"><?php echo $u['func_email']; ?></td>
						<td style="vertical-align: middle;"><?php echo $u['func_setor']; ?></td>
						<td style="vertical-align: middle;"><?php echo $u['func_cargo']; ?></td>
						<td style="vertical-align: middle;">
							<button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalEdit_<?php echo $u['func_id']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></button>
							<!-- Modal Alterar -->
							<div class="modal fade" id="myModalEdit_<?php echo $u['func_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<form action="atualizar.php" method="post" enctype="multipart/form-data">
										<div class="modal-header text-left">
											<h3 class="modal-title" id="myModalLabel"><img src="images/<?php echo $u['func_foto']; ?>" style="width: 64px;"> <strong><?php echo $u['func_nome']; ?> [<?php echo $u['func_setor']; ?>]</strong></h3>
										</div>
										<div class="modal-body text-left">
											<div class="form-group">
												<label for="exampleInputPassword1">Nome</label>
												<input type="text" class="form-control" name="func_nome" value="<?php echo $u['func_nome']; ?>">
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">E-mail</label>
												<input type="text" class="form-control" name="func_email" value="<?php echo $u['func_email']; ?>">
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">Setor</label>
												<input type="text" class="form-control" name="func_setor" value="<?php echo $u['func_setor']; ?>">
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">Cargo</label>
												<input type="text" class="form-control" name="func_cargo" value="<?php echo $u['func_cargo']; ?>">
											</div>
											<div class="form-group">
												<label for="exampleInputPassword1">Atualizar Foto <span style="color: #999; font-size: 8.5pt;"><em>(Só será atualizado caso escolha uma nova foto)</em></span></label>
												<input type="file" class="form-control" name="func_foto">
											</div>
											<input type="hidden" value="<?php echo $u['func_id']; ?>" name="func_id">
										</div>
										<div class="modal-footer label-default">
											<button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Sair</button>
											<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Atualizar</button>
										</div>
										</form>
									</div>
								</div>
							</div>

							<button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#myModal_<?php echo $u['func_id']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></button>
							<!-- Modal Deletar -->
							<div class="modal fade" id="myModal_<?php echo $u['func_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header label-danger">
											<h3 class="modal-title" id="myModalLabel" style="color: #FFF;"><strong>Atenção!</strong></h3>
										</div>
										<div class="modal-body alert-danger">
											<h3>Deseja realmente excluir o funcionário <strong><?php echo utf8_encode($u['func_nome']); ?></strong></h3>
											<h4><em class="text-danger">Essa ação é irreversível.</em></h4>
											<h4>
												<button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Sair</button>
												<a href="deletar.php?id=<?php echo $u['func_id']; ?>"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Deletar</button></a>
											</h4>
										</div>
										<div class="modal-footer label-danger">											
										</div>
									</div>
								</div>
							</div>

						</td>
					</tr>
					<?php
				}
			?>
			</tbody>
		</table>
	</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>