<?php
	session_start();
	set_time_limit(0);
	ini_set('post_max_size', '10M');
	ini_set('upload_max_filesize', '10M');

	require "banco.php";
	$sql = new banco("127.0.0.1","root","","sistema_funcionarios");

	#Verifica se o Cliente que está logado está com seu código correto e da acesso a
	#página administrativa, caso contrario retorna para tela de login.
	if(!$_SESSION['SISTFUNC2015SYSTEM']) {
		header("Location: index.php");
		die;
	}
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sistema de Funcionários</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="funcionarios.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Lista de Funcionários</a></li>
				<li class="label-success"><a href="cadastro.php" style="color: #FFF;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Novo Funcionário</a></li>				
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="label-danger"><a href="logout.php" style="color: #FFF;"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair</a></li>
			</ul>
		</div>
	</div>
</nav>


<div class="container">
	<div class="col-lg-5">
	<form action="acadastro_sucesso.php" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="exampleInputPassword1">Nome</label>
			<input type="text" class="form-control" name="func_nome">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">E-mail</label>
			<input type="text" class="form-control" name="func_email">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Setor</label>
			<input type="text" class="form-control" name="func_setor">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Cargo</label>
			<input type="text" class="form-control" name="func_cargo">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Foto</label>
			<input type="file" class="form-control" name="func_foto">
		</div>
		<button type="submit" class="btn btn-success form-control" name="submit"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Cadastrar</button>
	</form>
	</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>