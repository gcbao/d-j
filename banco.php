<?php
	class banco
	{
	    protected $servidor;
	    protected $usuario;
	    protected $senha;
	    protected $banco;
	    
	    #CONSTRUTOR QUE RECEBE OS PARAMETROS   
	    function __construct($servidor,$usuario,$senha,$banco)
	    {
	        $this->servidor = $servidor;
	        $this->usuario  = $usuario;
	        $this->senha    = $senha;
	        $this->banco    = $banco;
	    }

	    #CONECTA COM O BANCO DE DADOS
	    public function AcessoBanco()
	    {
	        $con = mysqli_connect($this->servidor, $this->usuario, $this->senha, $this->banco) or die("N&atilde;o foi poss&iacute;vel conectar-se com o banco de dados"); 
	        return $con;
	    }

	    public function acessarSistema($nomeLogin,$senhaLogin)
		{
			$s_user = "SELECT ac_id, ac_email, ac_senha FROM tbl_acesso";
			$q_user = mysqli_query($this->AcessoBanco(),$s_user);

			while ($u = mysqli_fetch_array($q_user)) 
			{
				$acId 		= $u['ac_id'];			
				$acUsuario 	= $u['ac_email'];
				$acSenha	= $u['ac_senha'];
				
				if ($nomeLogin == $acUsuario && md5($senhaLogin) == $acSenha)
				{
					$_SESSION["SISTFUNC2015SYSTEM"] = true;
					$_SESSION["SISTFUNC2015SYSTEM"] = array($acId,$acUsuario,$acSenha);
					header("Location:funcionarios.php");
					die;
				}
				else 
				{
				}
			}
		}
	}
?>